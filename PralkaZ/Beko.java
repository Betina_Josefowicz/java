package Zaliczenie.PralkaZ;

public class Beko extends Pralka {
	
	public Beko(int number, double temp, int spinSpeed) throws ProgramException {
		super(number, temp, spinSpeed, 1.0, Marki.BEKO.getName());
	}
	
	public Beko() throws ProgramException {
		super(1.0, Marki.BEKO.getName());
	}
}
