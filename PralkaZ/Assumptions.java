package Zaliczenie.PralkaZ;

public interface Assumptions {
	
	// metoda potrzebne do sortowania pralek po nazwie
	
	public String getName();
	
	// metody dot. programu prania
	public int getProgram();
	
	public void setProgram(int set_nr) throws ProgramException;
	
	public void nextProgram();
	
	public void previusProgram();
	
	public void displayCurrentProgNr();
	
	// metody dot. temperatury prania
	public double getTemp();
	
	public void setTemp(double temp_s);
	
	public void tempUp();
	
	public void tempDown();
	
	public void displayCurrentTemp();
	
	// metody dotyczace wirowania
	public void upV();
	
	public void downV();
	
	public void displayCurrentSpinSpeed();
	
	// metoda wyswietlajaca status pralki
	public void showStatus();
}
