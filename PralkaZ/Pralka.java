package Zaliczenie.PralkaZ;

public abstract class Pralka implements Assumptions {
	
	private int				program			= 1;
	private double			temp			= 30;
	private int				spinSpeed		= 500;
	private final int		MAX_NR_PROGRAM;
	private final double	STEP;
	private final String	name;
	private final int		TEMP_MAX		= 90;
	private final int		TEMP_MIN		= 0;
	private final int		SPIN_SPEED_MAX	= 1000;
	private final int		SPIN_SPEED_MIN	= 0;
	private final int		SPIN_STEP		= 100;
	
	// constructors
	public Pralka(int programC, double tempC, int spinSpeed, String name) throws ProgramException {
		
		this.MAX_NR_PROGRAM = 20;
		this.STEP = 0.5;
		try {
			setProgram(programC);
		} catch (ProgramException p) {
			p.getMessage();
			throw new ProgramException();
		}
		;
		setTemp(tempC);
		setspinSpeed(spinSpeed);
		
		this.name = name;
	}
	
	public Pralka(int programC, double tempC, int spinSpeed, double STEP, String name) throws ProgramException {
		this.MAX_NR_PROGRAM = 20;
		this.STEP = STEP;
		this.name = name;
		try {
			setProgram(programC);
		} catch (ProgramException p) {
			p.getMessage();
			throw new ProgramException();
		}
		;
		setTemp(tempC);
		setspinSpeed(spinSpeed);
		
	}
	
	public Pralka(int programC, double tempC, int spinSpeed, int MAX_NR_PROGRAM, String name) throws ProgramException {
		this.MAX_NR_PROGRAM = MAX_NR_PROGRAM;
		this.STEP = 0.5;
		this.name = name;
		
		try {
			setProgram(programC);
		} catch (ProgramException p) {
			p.getMessage();
			throw new ProgramException();
		}
		;
		setTemp(tempC);
		setspinSpeed(spinSpeed);
		
	}
	
	public Pralka(String name) throws ProgramException {
		this(1, 30.0, 500, name);
	}
	
	public Pralka(double STEP, String name) throws ProgramException {
		this(1, 30.0, 500, STEP, name);
	}
	
	public Pralka(int MAX_NR_PROGRAM, String name) throws ProgramException {
		this(1, 30.0, 500, MAX_NR_PROGRAM, name);
	}
	
	public String getName() {
		return name;
	}
	
	// program
	
	public int getProgram() {
		return program;
	}
	
	public void setProgram(int setNr) throws ProgramException {
		if (setNr <= this.MAX_NR_PROGRAM && setNr > 0) {
			this.program = setNr;
		} else {
			throw new ProgramException();
		}
	}
	
	// program step
	
	public void nextProgram() {
		int nr_programu = this.getProgram();
		nr_programu += 1;
		try {
			this.setProgram(nr_programu);
		} catch (ProgramException p) {
			System.out.println(p.getMessage() + "\nKolejny numer programu nie istnieje!");
		}
		displayCurrentProgNr();
	}
	
	public void previusProgram() {
		int programNr = this.getProgram();
		programNr -= 1;
		try {
			this.setProgram(programNr);
		} catch (ProgramException p) {
			System.out.println(p.getMessage() + "\nPoprzedni numer programu nie istnieje!");
		}
		displayCurrentProgNr();
	}
	
	public void displayCurrentProgNr() {
		System.out.println("Aktualny nr programu:" + this.getProgram());
	}
	
	// temp
	
	public double getTemp() {
		return temp;
	}
	
	public void setTemp(double tempS) {
		if (tempS <= TEMP_MAX && tempS >= TEMP_MIN) {
			double tempF = Math.round(tempS * 2) / 2.0;
			this.temp = tempF;
		} else {
			System.out.println("Wprowadzono wartość spoza zakresu temperatury, wprowadz wartość 0-90\u00B0C ");
		}
	}
	
	// temp step
	
	public void tempUp() {
		double currentTemp = this.getTemp();
		currentTemp += STEP;
		this.setTemp(currentTemp);
		displayCurrentTemp();
	}
	
	public void tempDown() {
		double currentTemp = this.getTemp();
		currentTemp -= STEP;
		this.setTemp(currentTemp);
		displayCurrentTemp();
	}
	
	public void displayCurrentTemp() {
		System.out.println("Aktualna temp to:" + this.getTemp() + "\u00B0C");
	}
	
	// spin
	
	private int getspinSpeed() {
		return spinSpeed;
	}
	
	public void setspinSpeed(int spinSpeed) {
		if (spinSpeed % SPIN_STEP == 0 && spinSpeed <= SPIN_SPEED_MAX && spinSpeed >= 0) {
			this.spinSpeed = spinSpeed;
		} else {
			System.out.println("Wprowadzona wartość predkosci wirowania nie jest podzielna przez:" + SPIN_STEP + ", lub podano wartość spoza zakresu.");
			displayCurrentSpinSpeed();
		}
		
	}
	
	// spin step
	public void upV() {
		int currentSpinSpeed = this.getspinSpeed();
		
		if (currentSpinSpeed >= SPIN_SPEED_MAX) {
			currentSpinSpeed = SPIN_SPEED_MIN;
		} else {
			currentSpinSpeed += SPIN_STEP;
		}
		this.setspinSpeed(currentSpinSpeed);
		displayCurrentSpinSpeed();
	}
	
	public void downV() {
		int currentSpinSpeed = this.getspinSpeed();
		if (currentSpinSpeed == SPIN_SPEED_MIN) {
			currentSpinSpeed = SPIN_SPEED_MAX;
		} else {
			currentSpinSpeed -= SPIN_STEP;
		}
		this.setspinSpeed(currentSpinSpeed);
		displayCurrentSpinSpeed();
	}
	
	public void displayCurrentSpinSpeed() {
		System.out.println("Aktualna predkosc wirowania to:" + this.getspinSpeed());
	}
	
	// status
	public void showStatus() {
		System.out.println("Status: nazwa pralki:\"" + this.name + "\" numer programu:" + this.program + " temperatura:" + this.temp + "\u00B0C" + " prędkość wirowania:" + this.spinSpeed);
	}
	
}
