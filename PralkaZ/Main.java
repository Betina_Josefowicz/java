package Zaliczenie.PralkaZ;

import java.util.ArrayList;
import java.util.List;

public class Main {
	
	public static void main(String[] args) throws ProgramException {
		// TASK Auto-generated method stub
		
		try {
			Pralka pralka1 = new Amica(20, 10.0, 1200);
			Pralka pralka2 = new Whirlpool();
			Pralka pralka3 = new Beko(20, 20.0, 400);
			
			try {
				pralka1.setProgram(21);
				pralka2.setProgram(21);
				pralka3.setProgram(25);
			} catch (ProgramException p) {
				System.out.println(p.getMessage() + "\nProbujesz wybrac wartosc spoza zakresu");
			}
			
			System.out.println();
			
			pralka1.setProgram(15);
			pralka1.previusProgram();
			pralka1.nextProgram();
			
			pralka1.setTemp(23.61);
			pralka1.tempUp();
			pralka1.tempDown();
			
			pralka1.setspinSpeed(600);
			pralka1.upV();
			pralka1.downV();
			
			pralka1.showStatus();
			
			System.out.println();
			
			pralka2.setProgram(24);
			pralka2.previusProgram();
			pralka2.nextProgram();
			
			pralka2.setTemp(90);
			pralka2.tempUp();
			pralka2.tempDown();
			
			pralka2.setspinSpeed(1000);
			pralka2.upV();
			pralka2.downV();
			
			pralka2.showStatus();
			
			System.out.println();
			
			pralka3.setProgram(1);
			pralka3.previusProgram();
			pralka3.nextProgram();
			
			pralka3.setTemp(0);
			pralka3.tempDown();
			pralka3.tempUp();
			
			pralka3.setspinSpeed(0);
			pralka3.downV();
			pralka3.upV();
			
			pralka3.showStatus();
			
		} catch (ProgramException p) {
			System.out.println(p.getMessage() + "\nBłedne parametry obiektu");
		}
		
		List<Pralka> listaPralek = new ArrayList<Pralka>();
		listaPralek.add(new Beko(12, 2.0, 100));
		listaPralek.add(new Whirlpool(12, 2.0, 100));
		listaPralek.add(new Amica(12, 2.0, 100));
		
		SortPralki.sortPralki(listaPralek);
		
	}
	
}
