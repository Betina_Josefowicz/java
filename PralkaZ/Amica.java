package Zaliczenie.PralkaZ;

public class Amica extends Pralka {
	
	public Amica(int number, double temp, int spinSpeed) throws ProgramException {
		super(number, temp, spinSpeed, Marki.AMICA.getName());
	}
	
	public Amica() throws ProgramException {
		super(Marki.AMICA.getName());
	}
}