package Zaliczenie.PralkaZ;

import java.util.List;

public class SortPralki {
	
	public static void sortPralki(List<Pralka> listaPralek) {
		System.out.println("\nLista pralek przed sortowaniem:");
		listaPralek.stream()
				.forEach(pralka -> System.out.print(pralka.getName() + ", "));
		
		System.out.println("\n\nLista pralek po sortowaniu:");
		listaPralek.stream()
				.sorted((a, b) -> a.getName()
						.compareTo(b.getName()))
				.forEach(pralka -> System.out.print(pralka.getName() + ", "));
		
	}
}
