package Zaliczenie.PralkaZ;

public class Whirlpool extends Pralka {
	
	public Whirlpool(int numer, double temp, int spinSpeed) throws ProgramException {
		super(numer, temp, spinSpeed, 24, Marki.WHIRLPOOL.getName());
	}
	
	public Whirlpool() throws ProgramException {
		super(24, Marki.WHIRLPOOL.getName());
	}
}
