package Zaliczenie.PralkaZ;

public enum Marki {
	
	BEKO("Beko"),
	WHIRLPOOL("Whirlpool"),
	AMICA("Amica");
	
	private String name;
	
	public String getName() {
		return name;
	}
	
	Marki(String name) {
		this.name = name;
	}
	
}
